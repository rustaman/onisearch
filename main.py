"""
File : main.py
Author : Amin - CDL
Version 0.1
"""

from messagehandler import MessageHandler


def main():
    mh = MessageHandler()
    token = ""
    with open("token.txt", "r") as token_file:
        token = token_file.read()
    mh.run(token)


if __name__ == '__main__':
    main()
