# Onisearch

Onisearch is a discord bot created for an alumni high school association to help students find post high school education.

It makes calls to the [Onisep search endpoint](https://www.onisep.fr/recherche?context=formation) and parses the response to extract the formations and degrees list.

# Build and run the container
```
docker build -t onisearch-bot .
```
Paste your bot token in a token.txt file

```
docker run -dit --name onisearch -v /path/to/token.txt:/onisearch/token.txt onisearch-bot
```