"""
File : oniseprequest.py
Author : Amin - CDL
Version 0.1
"""

import requests
import re
from bs4 import BeautifulSoup


class OnisepException(Exception):
    pass


class OnisepRequest:

    def __init__(self):
        self._offset = 0
        self._search_url = "https://www.onisep.fr/content/search/(offset)/{}?&SubTreeArray=164330&formationRecherche=" \
                           "1&zone_geo_F=1&limit=10&SearchText={}&filters[attr_niveau_enseignement_t][]=8"
        self._last_search_text = ""

    @staticmethod
    def _parse_results(results: bytes):

        soup = BeautifulSoup(results, 'html.parser')
        soup = soup.find("tbody")
        soup = BeautifulSoup(str(soup), "html.parser")
        soup = soup.find_all("tr")

        results = []
        for tr in soup:
            try:
                interm_soup = BeautifulSoup(str(tr), 'html.parser')
                interm_results = interm_soup.find_all("td")
                formation_title = str(interm_results[0].contents[1].contents[0])
                formation_link = "https://www.onisep.fr" + str(interm_results[0].contents[1].attrs['href'])
            except Exception:
                continue
            try:
                formation_level = str(interm_results[1].contents[1].contents[0])
            except IndexError:
                formation_level = ""

            results.append([formation_title, formation_link, formation_level])

        return results

    def search(self, text: str):

        # Sanitizing the search input
        text = re.sub(r'[^ \nA-Za-z0-9À-ÖØ-öø-ÿ/]+', '', text)
        text = text.replace(" ", "+")

        # Affecting the search input value for further use
        self._last_search_text = text
        self._offset = 0

        # Formating the search url with including the search parameters and performing the search
        search_url = self._search_url.format(self._offset, self._last_search_text)
        response = requests.get(search_url)

        if response.status_code == 200:

            # Parsing the response content to retrieve the result list
            result_list = self._parse_results(response.content)

            return result_list

        else:
            raise OnisepException(response.status_code)

    def next(self):
        if self._last_search_text == "":
            return []
        # Formating the search url with including the search parameters and performing the search
        search_url = self._search_url.format(self._offset + 10, self._last_search_text)
        response = requests.get(search_url)

        if response.status_code == 200:

            # Parsing the response content to retrieve the result list
            result_list = self._parse_results(response.content)

            self._offset = self._offset + 10 if len(result_list) > 0 else self._offset

            return result_list

        else:
            raise OnisepException(response.status_code)

    def previous(self):
        if self._last_search_text == "":
            return []
        # Formating the search url with including the search parameters and performing the search
        search_url = self._search_url.format(self._offset - 10, self._last_search_text)
        response = requests.get(search_url)

        if response.status_code == 200:

            # Parsing the response content to retrieve the result list
            result_list = self._parse_results(response.content)

            self._offset = self._offset - 10 if len(result_list) > 0 else self._offset

            return result_list

        else:
            raise OnisepException(response.status_code)
