FROM python:3.7-buster

run mkdir /onisearch

COPY main.py /onisearch
COPY messagehandler.py /onisearch
COPY oniseprequest.py /onisearch

COPY requirements.txt /onisearch

workdir /onisearch

RUN pip install --use-feature=2020-resolver -r ./requirements.txt

ENTRYPOINT [ "python", "./main.py"]